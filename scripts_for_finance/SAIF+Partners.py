import collections
import pymongo
from datetime import datetime
import csv
client = pymongo.MongoClient('localhost', 27017)
db = client['ziploan']

ziploan_loans_datas = db.loan_request_term_loan_repayment_details.aggregate(
[
{'$lookup': {'foreignField': 'loan_request_id', 'as': 'loan_disbursement_details', 'from': 'loan_request_disbursement_details', 'localField': 'loan_request_id'}},
{'$lookup': {'foreignField': '_id', 'as': 'loan_request_data', 'from': 'loan_request', 'localField': 'loan_request_id'}},  
{'$lookup': {'foreignField': 'loan_request_id', 'as': 'business_details', 'from': 'loan_request_business_info', 'localField': 'loan_request_id'}},
{'$lookup': {'foreignField': 'loan_request_id', 'as': 'personal_details', 'from': 'loan_request_personal_info', 'localField': 'loan_request_id'}},
{'$unwind': '$personal_details'},
{'$lookup': {'foreignField': 'loan_Request_Personal_Info_ID', 'as': 'cibil_data', 'from': 'loan_request_consumer_cibil_personal_data', 'localField': 'personal_details._id'}},
{'$lookup': {'foreignField': 'loan_request_id','as': 'risk_category_details','from': 'loan_request_zipscore_scorecard_feature_results','localField': 'loan_request_id'}},
{'$lookup': {'foreignField': 'loan_request_id','as': 'servicable_emi_data','from': 'loan_request_serviceable_emi','localField': 'loan_request_id'}},
]
)

businesses = {bc['_id']: bc['category_name'] for bc in db.business_category.find({'is_active': 1})}
loans = []

def create_emis(customer_details, ziploan_loans_data, action):
    for i in range(1,25):
        customer_details[action + ' EMI ' + str(i)] = '' 
    if action == 'Date Paid':
        for index, each in enumerate(ziploan_loans_data['instalment_list']):
            if each['instalment_status'] in [1,2]:
                if each['collection']:
                    customer_details[action + ' EMI '+str(index+1)] = datetime.strftime(each['collection'][-1]['date'], '%d/%m/%Y')
                else:
                    customer_details[action + ' EMI '+str(index+1)] = datetime.strftime(datetime.strptime(each['due_date'],'%d-%m-%Y'), '%d/%m/%Y')
            else:
                customer_details[action + ' EMI '+str(index+1)] = ''
    elif action == 'Amount Paid':
        for index, each in enumerate(ziploan_loans_data['instalment_list']):
            if each['instalment_status'] in [1,2]:
                if each['collection']:
                    paisa_collected = 0
                    for collection in each['collection']:
                        if 'amount_adjusted_against_interest_component' in collection:
                            paisa_collected += collection['amount_adjusted_against_interest_component']
                        if 'amount_adjusted_against_principal_component' in collection:
                            paisa_collected += collection['amount_adjusted_against_principal_component']
                        if 'amount_adjusted_against_overdue_interest_charges' in collection:
                            paisa_collected += collection['amount_adjusted_against_overdue_interest_charges']
                        if 'amount_adjusted_against_bouncing_charges' in collection:
                            paisa_collected += collection['amount_adjusted_against_bouncing_charges']
                    customer_details[action + ' EMI '+str(index+1)] = paisa_collected
                else:
                    customer_details[action + ' EMI '+str(index+1)] = ''
            else:
                customer_details[action + ' EMI '+str(index+1)] = ''
    elif action == 'Due Date':
        for index, each in enumerate(ziploan_loans_data['instalment_list']):
            customer_details[action + ' EMI '+str(index+1)] = datetime.strftime(datetime.strptime(each['due_date'],'%d-%m-%Y'), '%d/%m/%Y')
    elif action == 'Amount Due':
        for index, each in enumerate(ziploan_loans_data['instalment_list']):
            customer_details[action + ' EMI '+str(index+1)] = each['principal_component'] + each['interest_component']
    return customer_details

from dateutil.relativedelta import relativedelta
from dateutil import parser
for ziploan_loans_data in ziploan_loans_datas:
    customer_details = collections.OrderedDict()
    customer_details['Loan ID'] = ziploan_loans_data['loan_request_data'][0]['loan_application_number']
    customer_details['Customer ID'] = ''
    customer_details['CIBIL Score'] = ziploan_loans_data['cibil_data'][0]['score'][0]['score']
    customer_details['Ziploan Score'] = ziploan_loans_data['risk_category_details'][0]['zipscore']
    customer_details['Age'] = relativedelta(datetime.now(), datetime.strptime(ziploan_loans_data['personal_details']['information'][0]['date_of_birth'],'%d/%m/%Y')).years
    customer_details['Business Segment'] = businesses[ziploan_loans_data['business_details'][0]['business_category_id']]
    customer_details['Monthly Income'] = ziploan_loans_data['servicable_emi_data'][0]['monthly_income']
    customer_details['Annualized Credit'] = int(ziploan_loans_data['servicable_emi_data'][0]['annualised_business_credit'])
    approval_date = ''
    for each in ziploan_loans_data['loan_request_data'][0]['status_date']:
        if each['status'] == 5:
            approval_date = datetime.strftime(each['date'], '%d/%m/%Y')
    customer_details['Approval Date'] = approval_date
    disb = datetime.strftime(datetime.strptime(ziploan_loans_data['loan_disbursement_details'][0]['disbursement_date'], '%d-%m-%Y'), '%d/%m/%Y')
    customer_details['Disbursal Date'] = disb
    customer_details['Channel'] = 'Indirect' if ziploan_loans_data['loan_request_data'][0]['sourcing_type'] == 1 else 'Direct'
    customer_details['ROI'] = ziploan_loans_data['loan_disbursement_details'][0]['reducing_interest_rate_per_annum']
    customer_details['Tenor'] = ziploan_loans_data['loan_disbursement_details'][0]['tenor_in_months']
    customer_details['Loan Amount'] = ziploan_loans_data['loan_disbursement_details'][0]['total_loan_amount']
    customer_details['EMI'] = ziploan_loans_data['loan_disbursement_details'][0]['total_emi']
    emi_start_ddate = datetime.strftime(datetime.strptime(ziploan_loans_data['loan_disbursement_details'][0]['emi_start_date'], '%d-%m-%Y'), '%d/%m/%Y')
    customer_details['Date of first EMI'] = emi_start_ddate
    foreclosed, foreclosed_date = 'No', ''
    if 'loan_request_preclosure' in ziploan_loans_data or 'closing_date' in ziploan_loans_data:
        foreclosed = 'Yes'
        if 'closing_date' in ziploan_loans_data:
            foreclosed_date = datetime.strftime(ziploan_loans_data['closing_date'], '%d/%m/%Y')
        else:
            foreclosed_date = datetime.strftime(ziploan_loans_data['loan_request_preclosure']['pre_closing_date'], '%d/%m/%Y')
    customer_details['Foreclosed / Prepaid'] = foreclosed
    customer_details['Date of Foreclosure'] = foreclosed_date
    customer_details = create_emis(customer_details, ziploan_loans_data, 'Date Paid')
    customer_details = create_emis(customer_details, ziploan_loans_data, 'Amount Paid')
    customer_details = create_emis(customer_details, ziploan_loans_data, 'Due Date')
    customer_details = create_emis(customer_details, ziploan_loans_data, 'Amount Due')
    loans.append(customer_details)

from operator import itemgetter
newlist = sorted(loans, key=lambda x: datetime.strptime(x['Disbursal Date'],'%d/%m/%Y'))

keys = newlist[0].keys()
with open('Saif Partners Data.csv', 'wb') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(newlist)
print('finished writting file Saif Partners Data.csv')
