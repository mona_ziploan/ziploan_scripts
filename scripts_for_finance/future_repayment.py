from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_list = list()
loan_id_list = list()
max_length = 0
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": {"$gte": 10} }},

{"$lookup":
        {
          "from": "loan_request_disbursement_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "disbursement_details"
        }
 },
{ "$unwind" : "$disbursement_details" },
 
{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment"
        }
 },
{ "$unwind" : "$repayment"}
]):
  temp_dict = dict()
  max_length = 0
  temp_dict["loan_application_number"] = str(data['loan_application_number'])

  if 'disbursement_details' in data:
    temp_dict["loan_amount"] = str(data['disbursement_details']['total_loan_amount'])
    temp_dict["disbursement_date"] = str(data['disbursement_details']['disbursement_date'])
    temp_dict["tenor"] = str(data['disbursement_details']['tenor_in_months'])
    temp_dict["roi"] = str(data['disbursement_details']['reducing_interest_rate_per_annum'])
    temp_dict["emi"] = str(data['disbursement_details']['total_emi'])

  inst_list = list()
  if 'repayment' in data:
    for i in data['repayment']['instalment_list']:
      temp_dict1 = dict()
      """
      if datetime.now() >= convert(i['due_date']):
        continue
      """
      temp_dict1["due_date"] = str(i["due_date"])
      temp_dict1["interest_component"] = str(i["interest_component"])
      temp_dict1["principal_component"] = str(i["principal_component"])
      
      inst_list.append(temp_dict1)
  temp_dict["future_repayment_list"] = inst_list
  if len(inst_list)>max_length:
    max_length = len(inst_list)

  #print inst_list
  if temp_dict["loan_application_number"] not in loan_id_list:
    loan_id_list.append(temp_dict["loan_application_number"])
    temp_list.append(temp_dict)


with open('future_instalments.csv', 'wb') as csvfile:
  spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
  header = [' ',' ',' ',' ',' ','future_repayment']
  spamwriter.writerow(header)  
  header = ['loan_application_number','loan_amount','disbursement_date','roi','emi']
  for r in range(max_length):
    header+=['due_date','interest_component','principal_component']
    
  spamwriter.writerow(header)
  for k in temp_list:
    stri = list()
    stri=[k['loan_application_number']]+[k['loan_amount']]+[k['disbursement_date']]+[k['roi']]+[k['emi']]
    for i in k['future_repayment_list']:
      stri+=[i['due_date']]+[i['interest_component']]+[i['principal_component']]

    spamwriter.writerow(stri)

