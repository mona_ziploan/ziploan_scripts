from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

def obtain_cibil(loan_application_number):
	loan_application = db.loan_request.find_one({'loan_application_number': loan_application_number})
	if loan_application:
		personal_info = db.loan_request_personal_info.find_one({
			'loan_request_id': loan_application['_id']
			})
		if len(personal_info['information']) == 1:
			print loan_application_number, 'sole'
			# personal_info_id = personal_info['_id']
			# file_name = 'cibil_data/' + str(loan_application_number) + '.xml'
			# cibil_xml_response = db.loan_request_consumer_cibil_personal_data.find_one({
			# 	'loan_Request_Personal_Info_ID' : personal_info_id
			# 	})
			# f = open(file_name,'w+')
			# f.write(str(cibil_xml_response['cibil_xml_response']))
			# f.close()
		else:
			print loan_application_number, 'double'
	else:
		 print loan_application_number, 'latest'

loan_list = ['ZL19152101','ZL6532698','ZL52766358',' ZL24128557','ZL57794533','ZL78823405','ZL43656864','ZL1419086','ZL26272217','ZL76346791','ZL67910078','ZL10585879']
for i in loan_list:
	obtain_cibil(i)