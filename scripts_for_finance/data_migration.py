import MySQLdb
import pymongo
import traceback
from datetime import datetime, date
client = pymongo.MongoClient('localhost', 27017)
db = client['ziploan']

modes = {
	0 : 'Cash',
	1 : 'NACH',
	2 : 'Cheque',
	3 : 'IMPS',
	4 : 'RTGS',
	5 : 'NEFT',
	6 : 'E-Wallet'

}

def connection_to_db():
	#enter respective sql database details
	db1 = MySQLdb.connect(host="localhost",    # your host, usually localhost
						user="root",         # your username
						passwd="admin_root",  # your password
						db="ziploan_lm")        # name of the data base

	return db1

def convert(stri):
	return datetime.strptime(stri, "%Y-%m-%d")

def convert1(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

current_date = datetime.now()

def populate_loan_emi_schedule():
	emi_list = list()

	for data in db.loan_request.aggregate([
	{"$match": { "loan_request_status": {"$in":[10,11]} }},

	{"$lookup":
	        {
	          "from": "loan_request_disbursement_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "disbursement_details"
	        }
	 },
 	{'$unwind': {
	                'path': '$disbursement_details',
	                'preserveNullAndEmptyArrays': True
	            }
    },
	
	{"$lookup":
	        {
	          "from": "loan_request_term_loan_repayment_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "repayment"
	        }
	 },
	{'$unwind': {
	                'path': '$repayment',
	                'preserveNullAndEmptyArrays': True
	            }}
	]):
		
		schedule_id = 1
		flag = 0
		loan_no = data["loan_application_number"]
		if "repayment" in data:
			
			if "loan_request_preclosure" in data["repayment"]:
				flag = 1

			for instalments in data["repayment"]["instalment_list"]:
				if flag == 1 and instalments["instalment_status"] != 1:
					continue

				ref_id = str(instalments["_id"])
				#print data["_id"], instalments["due_date"]
				emi_tup = ()
				emi_due_date = str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
				emi_status = None
				interest_component = instalments["interest_component"]	
				principal_component = instalments["principal_component"]
				interest_component_overdue = instalments["instalment_interest_component_overdue"]
				principal_component_overdue = instalments["instalment_principal_component_overdue"]
				if "collection" in instalments:
					col_list =  instalments["collection"]

				if instalments["instalment_status"] == 1:
					emi_status = 1
					if len(col_list) == 0:
						last_collection_date = str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
						final_collection_date = str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
					else:
						col_item = col_list[-1]
						#print col_item
						if "status" in col_item:
							if col_item["status"] == 1 and col_item["mode"] == 1:
								last_collection_date =  str(datetime.strptime(col_item["nach_debit_date"],"%d-%m-%Y"))
								final_collection_date =  str(datetime.strptime(col_item["nach_debit_date"],"%d-%m-%Y"))

							elif col_item["status"] == 1 and col_item["mode"] == 2:
								if "realisation_date" in col_item:
									last_collection_date = str(col_item["realisation_date"])
									final_collection_date = str(col_item["realisation_date"])
								else:
									last_collection_date = str(col_item["date"])
									final_collection_date = str(col_item["date"])

							elif col_item["status"] == 1 and (col_item["mode"] != 1 or col_item["mode"] != 2):
								last_collection_date = str(col_item["date"])
								final_collection_date = str(col_item["date"])

						else:
							if "realisation_date" in col_item:
								last_collection_date = str(col_item["realisation_date"])
								final_collection_date = str(col_item["realisation_date"])
							else:
								last_collection_date = str(col_item["date"])
								final_collection_date = str(col_item["date"])

				elif instalments["instalment_status"] == 0:
					emi_status = 0
					last_collection_date = None
					final_collection_date = None
				
				elif instalments["instalment_status"] == 2:
					if instalments["instalment_interest_component_overdue"]+instalments["instalment_principal_component_overdue"]==instalments["interest_component"]+instalments["principal_component"]:
						emi_status = 0
						last_collection_date = None
						final_collection_date = None
					else:
						emi_status = 2
						for items in reversed(col_list):
							if "status" in items and items["status"] == 0:
								continue
							else:
								if "status" in col_item:
									if col_item["status"] == 1 and col_item["mode"] == 1:
										last_collection_date = str(datetime.strptime(col_item["nach_debit_date"],"%d-%m-%Y"))
										final_collection_date = None

									elif col_item["status"] == 1 and col_item["mode"] == 2:
										if "realisation_date" in col_item:
											last_collection_date = str(col_item["realisation_date"])
											final_collection_date = None
										else:
											last_collection_date = str(col_item["date"])
											final_collection_date = None

									elif col_item["status"] == 1 and (col_item["mode"] != 1 or col_item["mode"] != 2):
										last_collection_date = str(col_item["date"])
										final_collection_date = str(col_item["date"])


								else:
									last_collection_date = str(col_item["date"])
									final_collection_date = None

				emi_tup = (schedule_id, loan_no, emi_due_date, final_collection_date,emi_status,interest_component,principal_component,interest_component_overdue,principal_component_overdue,last_collection_date,ref_id)
				emi_list.append(emi_tup)

	try:
		db1 = connection_to_db()
		cur1 = db1.cursor()
		query = "insert into loan_emi_schedule(schedule_id,loan_no,emi_due_date,final_payment_date,emi_status,interest_component,principal_component,interest_component_overdue,principal_component_overdue,last_payment_date,old_ref_id) values (%s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s)"
		cur1.executemany(query,emi_list)
		db1.commit()
		cur1.close()
	except Exception as e:
		print "ERROR"
		print str(e)


def populate_collection_details():
	collection_list = list()
	reference_dict = dict()
	settlement_list = list()
	for data in db.loan_request.aggregate([
	{"$match": { "loan_request_status": {"$in":[10,11]} }},

	{"$lookup":
	        {
	          "from": "loan_request_disbursement_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "disbursement_details"
	        }
	 },
 	{'$unwind': {
	                'path': '$disbursement_details',
	                'preserveNullAndEmptyArrays': True
	            }
    },
	
	{"$lookup":
	        {
	          "from": "loan_request_term_loan_repayment_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "repayment"
	        }
	 },
	{'$unwind': {
	                'path': '$repayment',
	                'preserveNullAndEmptyArrays': True
	            }}
	]):

		loan_no = str(data["loan_application_number"])
		total_loan_amt = data["disbursement_details"]["total_loan_amount"]
		pos = total_loan_amt
		flag = 0
		if "repayment" in data:
			if "loan_request_preclosure" in data["repayment"]:
				flag = 1

			for instalments in data["repayment"]["instalment_list"]:
				if flag == 1 and instalments["instalment_status"] == 0:
					continue

				emi_ref_id = str(instalments["_id"])
				if instalments["instalment_status"] == 0:
					continue
				if "collection" not  in instalments:
					continue

				interest_component_residue = instalments["interest_component"] 
				principal_component_residue = instalments["principal_component"]

				# for settlment data
				emi_due_date = instalments["due_date"]

				for col_list in instalments["collection"]:
					if len(col_list) != 0:
						collection_amt = col_list.get("amount_adjusted_against_bouncing_charges",0)+col_list.get("amount_adjusted_against_overdue_interest_charges",0)+col_list.get("amount_adjusted_against_interest_component",0)+col_list.get("amount_adjusted_against_principal_component",0)
						collection_processed = 1
						collection_type = 1
						payment_mode = col_list["mode"]
						collection_flag = col_list.get("status",1)
						payment_collected_by = str(col_list.get("repayment_manager_id",''))
						
						# settlement data
						amount_adjusted_against_penalty = col_list.get("amount_adjusted_against_bouncing_charges",0)+col_list.get("amount_adjusted_against_overdue_interest_charges",0)
						amount_adjusted_against_principal_component = col_list.get("amount_adjusted_against_principal_component",0)
						amount_adjusted_against_interest_component = col_list.get("amount_adjusted_against_interest_component",0)
						interest_component_residue -= amount_adjusted_against_interest_component
						principal_component_residue -= amount_adjusted_against_principal_component
						pos -= amount_adjusted_against_principal_component
						if col_list["mode"] == 0:
							reference_no = str(col_list.get("reference_no",''))
							if "realization_date" in col_list:
								realization_date = str(col_list["realization_date"])
								collection_date = str(col_list["realization_date"])
							else:
								realization_date = str(col_list["date"])
								collection_date = str(col_list["date"])

							if "comments" in col_list:
								comments = str(col_list["comments"])
							else:
								comments = "payment through cash for emi "+str(instalments["due_date"])
							bank_name = col_list.get("bank_name",'')
						
						elif col_list["mode"] == 1:
							if "reference_no" in col_list:
								reference_no = str(col_list["reference_no"])
							else:
								reference_no = str(col_list.get("reserved_checksum",''))
							
							if "nach_debit_date" in col_list:
								realization_date = str(datetime.strptime(col_list["nach_debit_date"],'%d-%m-%Y'))
								collection_date= str(datetime.strptime(col_list["nach_debit_date"],'%d-%m-%Y'))
							else:
								realization_date = str(col_list.get("realization_date",col_list["date"]))
								collection_date= str(col_list.get("realization_date",col_list["date"]))

							comments = str(col_list.get("return_reason_description","payment through cash for emi "+str(instalments["due_date"])))

							
							bank_name = str(data["disbursement_details"]["repayment_bank_name"])
							
						
						elif col_list["mode"] == 2:
							reference_no = str(col_list.get("cheque_no",''))
							if "realization_date" in col_list:
								realization_date = str(col_list["realization_date"])
								collection_date = str(col_list["realization_date"])
							else:
								realization_date = str(col_list["date"])
								collection_date = str(col_list["date"])

							if col_list["status"] == 0:
								comments = str(col_list.get("return_reason_description","cheque bounced for emi "+str(instalments["due_date"])))
							else:
								comments = str(col_list.get("comments","payment through cheque for emi "+str(instalments["due_date"])))


							bank_name = str(col_list.get("bank_name",''))
						
						else:
							reference_no = str(col_list.get("reference_no",''))
							realization_date = str(col_list.get("realization_date",col_list['date']))
							collection_date = str(col_list.get("realization_date",col_list['date']))
							comments = str(col_list.get("comments","payment through cash for emi "+str(instalments["due_date"])))
							bank_name = str(col_list.get("bank_name",''))


					else:
						collection_amt = instalments.get("principal_component",0)+ instalments.get("interest_component",0)
						collection_processed = 1
						collection_type = 1
						payment_mode = 1
						collection_flag = instalments.get("instalment_status",1)
						payment_collected_by = str(col_list.get("repayment_manager_id",''))
						
						# settlement data
						amount_adjusted_against_penalty = 0
						amount_adjusted_against_principal_component = instalments.get("principal_component",0)
						amount_adjusted_against_interest_component = col_list.get("interest_component",0)
						interest_component_residue = instalments["interest_component"] - amount_adjusted_against_interest_component
						principal_component_residue = instalments["principal_component"] - amount_adjusted_against_principal_component
						pos -= amount_adjusted_against_principal_component
						reference_no = ''
						realization_date = str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
						collection_date= str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
						comments = "payment through cash for emi "+str(instalments["due_date"])
						bank_name = str(data["disbursement_details"]["repayment_bank_name"])

					if "reference_no" not in reference_dict:
						reference_list = list()
						reference_list = [round(float(collection_amt),2),collection_processed,collection_type,payment_mode,collection_flag,reference_no,realization_date,collection_date,comments,bank_name,loan_no,payment_collected_by]
						set_tuple = (reference_no,realization_date,loan_no,pos,amount_adjusted_against_penalty,amount_adjusted_against_principal_component,amount_adjusted_against_interest_component,interest_component_residue,principal_component_residue,emi_due_date,emi_ref_id)
						settlement_list.append(set_tuple)
						if reference_no == '':
							reference_no = str(reference_no+instalments["due_date"]+loan_no)

						reference_dict[str(reference_no)] = reference_list
					else:
						if reference_no == '':
							reference_no = str(reference_no+instalments["due_date"]+loan_no)

						set_tuple = (reference_no,realization_date,loan_no,pos,amount_adjusted_against_penalty,amount_adjusted_against_principal_component,amount_adjusted_against_interest_component,interest_component_residue,principal_component_residue,emi_due_date,emi_ref_id)
						settlement_list.append(set_tuple)
						reference_list = reference_dict[str(reference_no)]
						reference_list[0] += collection_amt
						reference_dict = reference_list	

	#reference dictionary position
	return reference_dict,settlement_list

					
			
		

def populate_collection_and_settlement_data():
	final_collection_list = list()
	reference_dict,settlement_list = populate_collection_details()
	for key, values in reference_dict.items():
		collection_tuple = ()
		collection_tuple = (values[10],values[0],values[7],values[4],values[3],values[5],values[6],values[8],values[9],values[1],values[2],values[11])
		final_collection_list.append(collection_tuple)

	try:
		db1 = connection_to_db()
		cur1 = db1.cursor()
		query = "insert into loan_collection_data(loan_no,collection_amt,collection_date,collection_flag,payment_mode,reference_no,realization_date,comments,bank_name,collection_processed,collection_type,payment_collected_by) values (%s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s)"
		cur1.executemany(query,final_collection_list)
		db1.commit()
		cur1.close()
	except Exception as e:
		print "ERROR"
		print str(e)


def populate_settlement_data(param):
	emi_dict = dict()
	col_dict = dict()
	final_settlement_list = list()
	if param == 1:
		reference_dict,settlement_list = populate_collection_details()
	elif param == 2:
		settlement_list = close_account_migration_emi_collection()
	print "#########################################"
	print settlement_list
	db1 = connection_to_db()
	cur3 = db1.cursor()
	cur4 = db1.cursor()
	query1 = """select emi_id, old_ref_id from loan_emi_schedule"""
	query2 = """select collection_id,loan_no,collection_date from loan_collection_data"""
	cur3.execute(query1)
	cur4.execute(query2)
	emi_data = cur3.fetchall()
	collection_data = cur4.fetchall()
	for i in emi_data:
		if i[1] not in emi_dict:
			emi_dict[str(i[1])] = str(i[0])
	
	for j in collection_data:
		nkey = str(j[1])+"P"+str(j[2])
		if nkey not in col_dict:
			col_dict[str(nkey)] = str(j[0])

	ct = 0
	for tup in settlement_list:
		print tup[2]
		set_tuple = ()
		emi_id = emi_dict[tup[10]]
		if str(tup[2]+"P"+str(tup[1])) in col_dict:
			collection_id = col_dict[str(tup[2]+"P"+str(tup[1]))]
		else:
			ct+=1
		set_tuple = (tup[2],emi_id,collection_id,tup[4],tup[5],tup[6],tup[1],tup[7],tup[8],tup[3])
		final_settlement_list.append(set_tuple)


	try:
		#print final_settlement_list
		#print ct
		db1 = connection_to_db()
		cur5 = db1.cursor()
		query5 = "insert into loan_emi_settlement_data(loan_no,emi_id,collection_id,amt_adjusted_against_penalty,amt_adjusted_against_interest_component,amt_adjusted_against_principal_component,payment_date,interest_component_residue,principal_component_residue,principal_outstanding) values (%s, %s, %s, %s, %s,%s,%s,%s,%s,%s)"
		cur5.executemany(query5,final_settlement_list)
		db1.commit()
		cur5.close()
	except Exception as e:
		print "ERROR"
		print str(e)



def close_account_migration_emi_collection():
	close_account_list = list()
	emi_list = list()
	collection_list = list()
	settlement_list = list()
	for data in db.loan_request.aggregate([
	{"$match": { "loan_request_status": {"$in":[11]} }},

	{"$lookup":
	        {
	          "from": "loan_request_disbursement_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "disbursement_details"
	        }
	 },
 	{'$unwind': {
	                'path': '$disbursement_details',
	                'preserveNullAndEmptyArrays': True
	            }
    },
	
	{"$lookup":
	        {
	          "from": "loan_request_term_loan_repayment_details",
	          "localField": "_id",
	          "foreignField": "loan_request_id",
	          "as": "repayment"
	        }
	 },
	{'$unwind': {
	                'path': '$repayment',
	                'preserveNullAndEmptyArrays': True
	            }}
	]):
		schedule_id = 1
		loan_no = data["loan_application_number"]
		collection_tup = ()
		pos = data["disbursement_details"]["total_loan_amount"]

		if "repayment" in data:
			if "loan_request_preclosure" in data["repayment"]:
				#total_amount_collected = data["repayment"]["total_closing_amount_collected"]
				transaction_date = str(data["repayment"]["loan_request_preclosure"]["pre_closing_date"])
				#### collection array 
				collection_amt = data["repayment"]["loan_request_preclosure"]["total_closing_amount_collected"]
				collection_processed = 1
				collection_type = 2
				payment_mode = data["repayment"]["loan_request_preclosure"]["mode_of_payment"]["type"]
				realization_date = transaction_date
				collection_date = realization_date
				reference_no = ' '
				collection_flag = 1
				payment_collected_by = ' '
				comments = ' '
				bank_name = ' '
				collection_tup = (loan_no,collection_amt,collection_date,collection_flag,payment_mode,reference_no,realization_date,comments,bank_name,collection_processed,collection_type,payment_collected_by)	
				collection_list.append(collection_tup)
				for instalments in data["repayment"]["instalment_list"]:
					emi_tup = ()
					set_tuple = ()

					if instalments["instalment_status"] == 1:
						pos-=instalments["principal_component"]
						continue

					emi_due_date = str(datetime.strptime(instalments["due_date"],"%d-%m-%Y"))
					final_collection_date = transaction_date
					last_payment_date = transaction_date
					emi_status = 1
					interest_component = instalments["interest_component"]
					principal_component = instalments["principal_component"]
					ref_id = str(instalments["_id"])
					interest_component_overdue = 0
					principal_component_overdue = 0
					emi_tup = (schedule_id, loan_no, emi_due_date, final_collection_date,emi_status,interest_component,principal_component,interest_component_overdue,principal_component_overdue,last_payment_date,ref_id)
					emi_list.append(emi_tup)
					# settlement data
					amount_adjusted_against_principal_component  = instalments.get("principal_component",0)
					amount_adjusted_against_interest_component = instalments.get("interest_component",0)
					interest_component_residue = 0
					principal_component_residue = 0
					pos-=amount_adjusted_against_principal_component

					if instalments["instalment_status"] == 0:
						amount_adjusted_against_penalty = 0
					elif instalments["instalment_status"] == 2:
						overdue_charges = 0
						dif_in_current_and_due_date = current_date - convert1(instalments["due_date"])
						difference2 = dif_in_current_and_due_date.days
						if difference2 > 15:
							overdue_charges = (difference2*2*(instalments["instalment_interest_component_overdue"]
                                            +instalments["instalment_principal_component_overdue"]))/(30*100)

						amount_adjusted_against_penalty = instalments.get("instalment_bouncing_charges_overdue",0)+overdue_charges

					set_tuple = (reference_no,realization_date,loan_no,pos,amount_adjusted_against_penalty,amount_adjusted_against_principal_component,amount_adjusted_against_interest_component,interest_component_residue,principal_component_residue,emi_due_date,ref_id)						
					settlement_list.append(set_tuple)
	print emi_list
	print collection_list
	try:
		db1 = connection_to_db()
		cur1 = db1.cursor()
		query = "insert into loan_emi_schedule(schedule_id,loan_no,emi_due_date,final_payment_date,emi_status,interest_component,principal_component,interest_component_overdue,principal_component_overdue,last_payment_date,old_ref_id) values (%s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s)"
		cur1.executemany(query,emi_list)
		db1.commit()
		cur1.close()

		db1 = connection_to_db()
		cur1 = db1.cursor()
		query = "insert into loan_collection_data(loan_no,collection_amt,collection_date,collection_flag,payment_mode,reference_no,realization_date,comments,bank_name,collection_processed,collection_type,payment_collected_by) values (%s, %s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s)"
		cur1.executemany(query,collection_list)
		db1.commit()
	except Exception as e:
		print "ERROR"
		print str(e)

	return settlement_list

"""
def build_loan_settlment_data():
	settlement_list = close_account_migration_emi_collection()
"""

##### first#####
#populate_loan_emi_schedule()
##### second ###########3
#populate_collection_and_settlement_data()
#### fourth ################
#populate_settlement_data(1)
##### third #######
#populate_settlement_data(2)

def find_dpd_data(given_date):
	db6 = connection_to_db()
	cur6 = db6.cursor()
	dpd_dict= dict()
	query_unset = """SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"""
	cur6.execute(query_unset)
	query_dpd = """select loan_no,min(emi_due_date),final_payment_date from loan_emi_schedule where (emi_due_date < %s) and (final_payment_date > %s or final_payment_date is NULL) group by loan_no"""
	cur6.execute(query_dpd,(given_date,given_date))
	dpd_data = cur6.fetchall()
	print dpd_data
	for i in dpd_data:
		if i[0] not in dpd_dict:
			dpd_dict[str(i[0])] = (convert(given_date)-i[1]).days

	print dpd_dict



def collections_data():
	final_list = list()
	final_list.append(["loan_no","collection_amt","collection_date"])
	db6 = connection_to_db()
	cur6 = db6.cursor()
	dpd_dict= dict()
	query_unset = """SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"""
	cur6.execute(query_unset)
	query_dpd = """select loan_no, collection_amt,collection_date from loan_collection_data order by loan_no,collection_date """
	cur6.execute(query_dpd)
	dpd_data = cur6.fetchall()
	for i in dpd_data:
		temp_list = [i[0],i[1],str(i[2])]
		final_list.append(temp_list)

	print final_list

find_dpd_data("2017-10-31")
#collections_data()

















		