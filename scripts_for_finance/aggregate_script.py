from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_dict = dict()
temp_list = list()
loan_id_list = list()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": 10 }},

{"$lookup":
        {
          "from": "loan_request_disbursement_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "disbursement_details"
        }
 },
 {'$unwind': {
                'path': '$disbursement_details',
                'preserveNullAndEmptyArrays': True
            }
            },
{"$lookup":
        {
          "from": "loan_request_zipscore_scorecard_feature_results",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "zipscore"
        }
 },
{'$unwind': {
                'path': '$zipscore',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "loan_request_serviceable_emi",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "servicable_emi"
        }
 },
{'$unwind': {
                'path': '$servicable_emi',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "loan_request_business_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "business_info"
        }
 },
{'$unwind': {
                'path': '$business_info',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "business_category",
          "localField": "business_info.business_category_id",
          "foreignField": "_id",
          "as": "category"
        }
 },
{'$unwind': {
                'path': '$category',
                'preserveNullAndEmptyArrays': True
            }},

{"$lookup":
        {
          "from": "entity_type",
          "localField": "business_info.entity_id",
          "foreignField": "_id",
          "as": "business_entity"
        }
 },
{'$unwind': {
                'path': '$business_entity',
                'preserveNullAndEmptyArrays': True
            }},
 
{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment"
        }
 },
{'$unwind': {
                'path': '$repayment',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "loan_request_personal_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "personal_info"
        }
 },
{'$unwind': {
                'path': '$personal_info',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "loan_request_consumer_cibil_personal_data",
          "localField": "personal_info._id",
          "foreignField": "loan_Request_Personal_Info_ID",
          "as": "scorecard"
        }
 },
{'$unwind': {
                'path': '$scorecard',
                'preserveNullAndEmptyArrays': True
            }},
{"$lookup":
        {
          "from": "partner_details",
          "localField": "partner_id",
          "foreignField": "_id",
          "as": "partner"
        }
 },
{'$unwind': {
                'path': '$partner',
                'preserveNullAndEmptyArrays': True
            }},

]):	
	temp_dict =  dict()
	temp_dict["loan_application_number"] = str(data['loan_application_number'])
	
	if 'personal_info' in data and 'information' in data['personal_info']:
		for ele in data['personal_info']['information']:
			#print ele
			if ele['applicant_type'] == 1:
				temp_dict["pan_primary"] = str(ele['pan'])
				temp_dict["customer_name1"] = str(ele['first_name'])+" "+str(ele['last_name'])
				temp_dict["gender_primary"] = str(ele['gender'])
		
			elif ele['applicant_type'] == 2:
				temp_dict["pan_secondary"] = str(ele['pan'])
				temp_dict["customer_name2"] = str(ele['first_name'])+" "+str(ele['last_name'])
				temp_dict["gender_secondary"] = str(ele['gender'])

	
	if  'business_info' in data:
		temp_dict["pan"] = str(data['business_info']['business_pan_no'])

		temp_dict["business_name"] = str(data['business_info']['business_name'])
		temp_dict["location"] = str(data['business_info']['business_address'])+" "+str(data['business_info']['business_city'])+" "+str(data['business_info']['business_state'])+" "+str(data['business_info']['business_pincode'])
		temp_dict["business_city"] = str(data['business_info']['business_city'])
		temp_dict["business_state"] = str(data['business_info']['business_state'])
	if "category" in data:
		temp_dict["business_category"] = str(data["category"]["category_name"])
	else:
		temp_dict["business_category"] = "category data not available"
	temp_dict["status"] = str(data['loan_request_status'])
	temp_dict["sourcing"] = str(data['sourcing_type'])
	if int(data['sourcing_type']) == 0:
		if 'direct_sourcing_type' in data:
			temp_dict["direct_source"] = str(data['direct_sourcing_type'])
	temp_dict["zipscore"] = str(data['zipscore']['zipscore'])
	temp_dict["loan_amount"] = str(data['disbursement_details']['total_loan_amount'])
	#print type(data['disbursement_details']['disbursement_date'])
	temp_dict["disbursal_date"] = str(data['disbursement_details']['disbursement_date'])
	temp_dict["loan_tenure"] = str(data['disbursement_details']['tenor_in_months'])
	temp_dict["emi_start_date"] = str(data['disbursement_details']["emi_start_date"])
	if 'processing_fee_amount' in data['disbursement_details']:
		temp_dict["processing_amount"] = str(data['disbursement_details']['processing_fee_amount'])
	temp_dict["roi"] = str(data['disbursement_details']['reducing_interest_rate_per_annum'])
	temp_dict["disburse_date"] = str(convert(str(data['disbursement_details']['disbursement_date'])).day)
	temp_dict["disburse_month"] = str(convert(str(data['disbursement_details']['disbursement_date'])).month)
	temp_dict["disburse_year"] = str(convert(str(data['disbursement_details']['disbursement_date'])).year)
	if 'lenders' in data:
		lenders = data['lenders'][-1]
		if 'ziploan_loan_amount_share' in lenders:
			if float(lenders["ziploan_loan_amount_share"]) == 0.2:
				temp_dict["ziploan_share"] = str(0.2*float(temp_dict['loan_amount']))
				temp_dict["partner_code"] = 'IDFC'
				temp_dict["Partner_share"] = str(0.8*float(temp_dict['loan_amount']))
			elif lenders["ziploan_loan_amount_share"] == 0:
				temp_dict["ziploan_share"] = 0
				temp_dict["partner_code"] = 'RBL'
				temp_dict["Partner_share"] = str(temp_dict['loan_amount'])
			temp_dict['partner_sanction_date'] = str(lenders['date'])
		else:
			temp_dict["ziploan_share"] = temp_dict['loan_amount']
			temp_dict["partner_code"] = 'NA'
			temp_dict["Partner_share"] = 'NA'
			temp_dict['partner_sanction_date'] = 'NA'
	else:
		temp_dict["ziploan_share"] = temp_dict['loan_amount']
		temp_dict["partner_code"] = 'NA'
		temp_dict["Partner_share"] = 'NA'
		temp_dict['partner_sanction_date'] = 'NA'
	
	if "scorecard" in data:
		if "score" in data["scorecard"]:
			temp_dict["cibil_score"] = str(data["scorecard"]["score"][0]["score"])
	else:
		temp_dict["cibil_score"] = "NA"

	if "repayment" in data:
		if "loan_request_preclosure" in data["repayment"]:
			temp_dict["closing_date"] = str(data["repayment"]["loan_request_preclosure"]["pre_closing_date"])
	else:
		temp_dict["closing_date"] = "NA"
	if "servicable_emi" in data:
		temp_dict["annualised_business_credit"] = data["servicable_emi"]["annualised_business_credit"]
		temp_dict["annual_income"] = data["servicable_emi"]["annual_income"]
		temp_dict["monthly_income"] = data["servicable_emi"]["monthly_income"]
	else:
		temp_dict["annualised_business_credit"] = "NA"
		temp_dict["annual_income"] = "NA"
		temp_dict["monthly_income"] = "NA"
	
	if "partner" in data:
		temp_dict["partner_name"] = str(data["partner"]["business_name"])
	else:
		temp_dict["partner_name"] = "NA"

	#print data["business_entity"]
	temp_dict['business_entity'] = str(data["business_entity"]["entity_type_name"])
	if temp_dict["loan_application_number"] not in loan_id_list:
		loan_id_list.append(temp_dict["loan_application_number"])
		temp_list.append(temp_dict)


print temp_list
