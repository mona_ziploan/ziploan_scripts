from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_list = list()
loan_id_dict = dict()
due_date_list = ['1:2016','2:2016','3:2016','4:2016','5:2016','6:2016','7:2016','8:2016','9:2016','10:2016','11:2016','12:2016','1:2017','2:2017','3:2017','4:2017','5:2017','6:2017','7:2017','8:2017','9:2017','10:2017','11:2017','12:2017']
ddlist = ['jan:2016','feb:2016','mar:2016','apr:2016','may:2016','june:2016','july:2016','aug:2016','sep:2016','oct:2016','nov:2016','dec:2016','jan:2017','feb:2017','mar:2017','apr:2017','may:2017','june:2017','jul:2017','aug:2017','sep:2017','oct:2017','nov:2017','dec:2017']
max_length = len(due_date_list)
ct = 0
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

with open('collection_data.csv', 'wb') as csvfile:
  spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
  header = [' ',' ',' ',' ',' ',' ','past_repayment']
  for i in ddlist:
    header+=[str(i),str(i),str(i)]
  print header

  spamwriter.writerow(header) 
  header = ['loan_application_number','disbursement_date','disbursement_amount','emi_start_date','emi_amount','closing_date','status']
  for r in range(max_length):
    header+=['interest','principal','penalties'] 
  spamwriter.writerow(header)


  for data in db.loan_request.aggregate([
  {"$match": { "loan_request_status": {"$gte": 10} }},

  {"$lookup":
          {
            "from": "loan_request_disbursement_details",
            "localField": "_id",
            "foreignField": "loan_request_id",
            "as": "disbursement_details"
          }
   },
  { "$unwind" : "$disbursement_details" },
  {"$lookup":
          {
            "from": "loan_request_term_loan_repayment_details",
            "localField": "_id",
            "foreignField": "loan_request_id",
            "as": "repayment"
          }
   },
  { "$unwind" : "$repayment"},
   
  {"$lookup":
          {
            "from": "loan_request_collection_records",
            "localField": "_id",
            "foreignField": "loan_request_id",
            "as": "collection_records"
          }
   },
  { "$unwind" : "$collection_records"},
  { "$unwind" : "$collection_records.collection_list"},
  {"$project":{"collection_records":1,"disbursement_details":1,"loan_application_number":1,"loan_request_status":1,"_id":1,"repayment":1}},
  {
    "$group" : {
       "_id" : { "loan_request_id":"$_id","month": { "$month": "$collection_records.collection_list.date" }, "year": { "$year": "$collection_records.collection_list.date" } },
     'data':
    {
        '$addToSet': "$$ROOT"
    }
    }
  }

  ]):
    temp_dict = dict()
    date_dict= dict()
    if str(data['_id']['loan_request_id']) not in loan_id_dict:
      loan_id_dict[str(data['_id']['loan_request_id'])] = list()
      temp = list()
      loan_application_number = str(data['data'][0]['loan_application_number'])
      if "disbursement_details" in data['data'][0] and 'disbursement_date' in data['data'][0]['disbursement_details'] :
        dis_date = str(data['data'][0]['disbursement_details']['disbursement_date'])
      else:
        dis_date = "Not found in DB"

      if "disbursement_details" in data['data'][0] and 'disbursement_amount' in data['data'][0]['disbursement_details'] :
        dis_amt = str(data['data'][0]['disbursement_details']['disbursement_amount'])
      else:
        dis_amt = "Not found in DB"

      if "disbursement_details" in data['data'][0] and 'emi_start_date' in data['data'][0]['disbursement_details'] :
        emi_start_date = str(data['data'][0]['disbursement_details']['emi_start_date'])
      else:
        emi_start_date = "Not found in DB"

      if "disbursement_details" in data['data'][0] and 'total_emi' in data['data'][0]['disbursement_details'] :
        emi = str(data['data'][0]['disbursement_details']['total_emi'])
      else:
        emi = "Not found in DB"
      if "repayment" in data['data'][0]:
        if "loan_request_preclosure" in data['data'][0]["repayment"]:
          closing_date = str(data['data'][0]["repayment"]["loan_request_preclosure"]["pre_closing_date"])
        else:
          closing_date = "NA"
      else:
          closing_date = "NA"
      
      temp =[loan_application_number,dis_date,dis_amt,emi_start_date,emi,closing_date]

      if data['data'][0]["loan_request_status"] == 10:
        temp.append("Live")
      elif data['data'][0]["loan_request_status"] == 11:
        temp.append("Closed")
      for dates in due_date_list:
        temp.extend([0,0,0])

      due = str(data['_id']['month'])+":"+str(data['_id']['year'])
      if due not in date_dict:
        date_dict[str(due)] = list()

      amount_adjusted_against_principal = 0
      amount_adjusted_against_interest = 0
      amount_adjusted_against_penalty = 0
      for i in data['data']:
        if 'collection_records' in i and 'collection_list' in i['collection_records']:
          col_list = i['collection_records']['collection_list']
          if ('amount_adjusted_against_interest_component' not in col_list  and 'amount_adjusted_against_principal_component' not in col_list) and (col_list['instalment_status']==1 or  col_list['instalment_status']==2) and ('amount_adjusted_against_bouncing_charges' in col_list or 'amount_adjusted_against_overdue_interest_charges' in col_list):
            if "status" in col_list:
              if int(col_list["status"]) == 0:
                continue

            amount_adjusted_against_principal+=col_list['principal_component']
            amount_adjusted_against_interest+= col_list['interest_component']
            continue
          if 'amount_adjusted_against_principal_component' in col_list:
            amount_adjusted_against_principal += col_list['amount_adjusted_against_principal_component']
          if 'amount_adjusted_against_interest_component' in col_list:
            amount_adjusted_against_interest += col_list['amount_adjusted_against_interest_component']
          if 'amount_adjusted_against_bouncing_charges' in col_list:
            amount_adjusted_against_penalty+= col_list['amount_adjusted_against_bouncing_charges']
          if 'amount_adjusted_against_overdue_interest_charges' in col_list:
            amount_adjusted_against_penalty+= col_list['amount_adjusted_against_overdue_interest_charges']
      #temp.extend([amount_adjusted_against_interest,amount_adjusted_against_principal,amount_adjusted_against_penalty])
      #date_dict[str(due)] = [amount_adjusted_against_interest,amount_adjusted_against_principal,amount_adjusted_against_penalty]
      ind = 7+3*due_date_list.index(due)
      temp[ind] = amount_adjusted_against_interest
      temp[ind+1] = amount_adjusted_against_principal
      temp[ind+2] = amount_adjusted_against_penalty
      loan_id_dict[str(data['_id']['loan_request_id'])] = temp
    else:
      temp = list()
      
      due = str(data['_id']['month'])+":"+str(data['_id']['year'])
      if due not in date_dict:
        date_dict[str(due)] = list()

      #temp.append(due)
      amount_adjusted_against_principal = 0
      amount_adjusted_against_interest = 0
      amount_adjusted_against_penalty = 0
      for i in data['data']:
        if 'collection_records' in i and 'collection_list' in i['collection_records']:
          col_list = i['collection_records']['collection_list']
          if ('amount_adjusted_against_interest_component' not in col_list  and 'amount_adjusted_against_principal_component' not in col_list) and (col_list['instalment_status']==1 or col_list['instalment_status']==2) and ('amount_adjusted_against_bouncing_charges' in col_list or 'amount_adjusted_against_overdue_interest_charges' in col_list):
            if "status" in col_list:
              if int(col_list["status"]) == 0:
                continue
            amount_adjusted_against_principal+=col_list['principal_component']
            amount_adjusted_against_interest+= col_list['interest_component']
            continue

          if 'amount_adjusted_against_principal_component' in col_list:
            amount_adjusted_against_principal += col_list['amount_adjusted_against_principal_component']
          if 'amount_adjusted_against_interest_component' in col_list:
            amount_adjusted_against_interest += col_list['amount_adjusted_against_interest_component']
          if 'amount_adjusted_against_bouncing_charges' in col_list:
            amount_adjusted_against_penalty+= col_list['amount_adjusted_against_bouncing_charges']
          if 'amount_adjusted_against_overdue_interest_charges' in col_list:
            amount_adjusted_against_penalty+= col_list['amount_adjusted_against_overdue_interest_charges']

      #temp.extend([amount_adjusted_against_interest,amount_adjusted_against_principal,amount_adjusted_against_penalty])
      #date_dict[str(due)] = [amount_adjusted_against_interest,amount_adjusted_against_principal,amount_adjusted_against_penalty]
      temp1 = loan_id_dict[str(data['_id']['loan_request_id'])]
      ind = 7+3*due_date_list.index(due)
      temp1[ind] = amount_adjusted_against_interest
      temp1[ind+1] = amount_adjusted_against_principal
      temp1[ind+2] = amount_adjusted_against_penalty
      #temp1.extend(temp)
      loan_id_dict[str(data['_id']['loan_request_id'])] = temp1


  
  for i in loan_id_dict:
    new_list1 = loan_id_dict[i]
    spamwriter.writerow(new_list1)




