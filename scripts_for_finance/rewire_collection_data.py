from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_list = list()
loan_id_list = list()
max_length = 0
currentdate= datetime.now().date()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": {"$gte": 10} }},
 
{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment"
        }
 },
{ "$unwind" : "$repayment"}
]):
  
  temp_dict = dict()
  collection_list = list()
  if "repayment" in data:
    for ilist in data['repayment']['instalment_list']:
      if convert(ilist['due_date']).date() < convert("30-06-2016").date():
        continue
      """
      if convert(ilist['due_date']).date() > currentdate:
        continue
      """
      ilist.pop('_id', None)
      
      if "collection" in ilist:
        temp = ilist["collection"]
        ilist.pop('collection', None)
        ilist.pop('comments',None)  
        ## adding due date to all collection arrays
        result = [dict(k, **ilist) for k in temp]

        collection_list.extend(result)

  temp_dict['loan_request_id'] = data['_id']
  temp_dict['collection_list'] = collection_list
  temp_list.append(temp_dict)

db.loan_request_collection_records.insert(temp_list)




