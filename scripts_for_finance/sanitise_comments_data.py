from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_dict = dict()
temp_list = list()
loan_id_list = list()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

cdate = datetime.now()
for data in db.loan_request_term_loan_repayment_details.aggregate([
	{"$project":{"loan_request_id":1,"instalment_list":1,"_id":0}},
	{"$lookup":
        {
          "from": "loan_request_repayment_comments",
          "localField": "loan_request_id",
          "foreignField": "loan_request_id",
          "as": "repayment_comments"
        }
 	},
	{'$unwind': {'path': '$repayment_comments', 'preserveNullAndEmptyArrays': True}}
	]):
	comments_array = list()
	comments_array_existing = list()
	if "instalment_list" in data:
		for com_data in data["instalment_list"]:
			if "comments" not in com_data:
				continue
			else:
				comments_array.extend(com_data["comments"])

	if "repayment_comments" in data:
		comments_array_existing = data["repayment_comments"]["comments"]


	comments_array.extend(comments_array_existing)
	db.loan_request_repayment_comments.update({"loan_request_id": data["loan_request_id"]},
                                                           {"$set":{"loan_request_id": data["loan_request_id"],"date":cdate,"comments": comments_array}},upsert=True)