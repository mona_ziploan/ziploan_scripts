from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_list = list()
loan_id_dict = dict()
due_date_list = ['1:2016','2:2016','3:2016','4:2016','5:2016','6:2016','7:2016','8:2016','9:2016','10:2016','11:2016','12:2016','1:2017','2:2017','3:2017','4:2017','5:2017','6:2017','7:2017','8:2017','9:2017','10:2017','11:2017','12:2017']
ddlist = ['jan:2016','feb:2016','mar:2016','apr:2016','may:2016','june:2016','july:2016','aug:2016','sep:2016','oct:2016','nov:2016','dec:2016','jan:2017','feb:2017','mar:2017','apr:2017','may:2017','june:2017','jul:2017','aug:2017','sep:2017','oct:2017','nov:2017','dec:2017']
max_length = len(due_date_list)
ct = 0
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

with open('overdue_data.csv', 'wb') as csvfile:
  spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
  header = [' ',' ',' ',' ',' ',' ','past_repayment']
  for i in ddlist:
    header+=[str(i),str(i),str(i)]
  print header

  spamwriter.writerow(header) 
  header = ['loan_application_number','disbursement_date','disbursement_amount','emi_start_date','emi_amount','closing_date','status']
  for r in range(max_length):
    header+=['interest','principal','penalties'] 
  spamwriter.writerow(header)


  for data in db.loan_request.aggregate([
  {"$match": { "loan_request_status": {"$gte": 10} }},   
  {"$lookup":
          {
            "from": "loan_request_collection_records",
            "localField": "_id",
            "foreignField": "loan_request_id",
            "as": "collection_records"
          }
   },
  { "$unwind" : "$collection_records"},
  { "$unwind" : "$collection_records.collection_list"},
  {"$project":{"collection_records":1,"disbursement_details":1,"loan_application_number":1,"loan_request_status":1,"_id":1,"repayment":1}},
  {
    "$group" : {
       "_id" : { "loan_request_id":"$_id","month": { "$month": "$collection_records.collection_list.date" }, "year": { "$year": "$collection_records.collection_list.date" } },
     'data':
    {
        '$addToSet': "$$ROOT"
    }
    }
  }

  ]):
    temp_dict = dict()
    if str(data['_id']['loan_request_id']) not in loan_id_dict:
      loan_id_dict[str(data['_id']['loan_request_id'])] = list()
      temp = list()
      loan_application_number = data['data'][0]["loan_application_number"]
      temp = [loan_application_number]
      for dates in due_date_list:
        temp.extend([0,0,0])

      if str(data['_id']['loan_request_id']) != "580095b72d10735a6b72b5f5":
        continue
      
      for col_data in data['data']:
        print col_data



