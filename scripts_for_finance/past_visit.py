from pymongo import MongoClient
from datetime import datetime
import traceback
from bson.objectid import ObjectId
import csv

connection = MongoClient()
db = connection.ziploan

def convert(stri):
	return datetime.strptime(stri, "%m/%d/%Y")

def past_visits():
	temp_list = list()
	with open('visits.csv', 'rb') as csvfile:
		freader = csv.reader(csvfile, delimiter=',', quotechar='|')
		for row in freader:
			temp_dict = dict()
			loan_application_number = row[0]
			temp_dict["loan_request_id"] = db.loan_request.find_one({"loan_application_number":loan_application_number})["_id"]
			temp_dict["date_of_visit"] = convert(row[1]).date()
			temp_dict["time_of_visit"] = convert(row[1]).time()
			temp_dict["asset_manager_id"] = ObjectId("59faebfca947f9d7379e4bbb")
			temp_dict["person_met"] = str(row[4])
			temp_dict["nature_of_work"] = str(row[5])
			temp_dict["app_uninstallation_reason"] = str(row[6])
			temp_dict["feedback"] = str(row[7])
			temp_dict["asset_manager_feedback"] = str(row[8])
			temp_dict["new_business_address"] = str(row[9])
			temp_dict["visited_business_address"] = str(row[10])
			temp_dict["primary_applicant_alternative_no"] = str(row[11])
			temp_dict["secondary_applicant_alternative_no"] = str(row[12])
			temp_dict["business_premise_availability"] = str(row[13])
			temp_dict["business_operational_at_given_business_address"] = str(row[14])
			temp_dict["employee_number_declaration"] = str(row[15])
			temp_dict["employee_number_on_site"]= str(row[16])
			temp_dict["sign_board_availability"] = str(row[17])
			temp_dict["raw_material_value_in_rupees"] = str(row[18])
			temp_dict["stock_or_inventory_in_rupees"] = str(row[19])
			temp_dict["asset_or_machinery_value_in_rupees"] = str(row[20])
			temp_dict["number_of_customers"] = str(row[21])
			temp_dict["average_debtor_cycle"] = str(row[22])
			temp_dict["average_creditor_cycle"] = str(row[23])
			temp_dict["recorded_coordinates"] = str(row[24])
			temp_dict["business_place_photo"] = str(row[25])
			temp_list.append(temp_dict)

	db.loan_request_asset_manager_visits.insert(temp_list)


past_visits()