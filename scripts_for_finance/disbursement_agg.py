from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_dict = dict()
temp_list = list()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": {"$gte": 10} }},


{"$lookup":
        {
          "from": "loan_request_disbursement_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "disbursement_details"
        }
 },
{ "$unwind" : "$disbursement_details" },

{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment_details"
        }
 },
{ "$unwind" : "$repayment_details" },

]):
  

  amount_collected = 0
  bouncing_charges_to_be_collected = 0

  if "repayment_details" in data:
    for rpdetails in data["repayment_details"]["instalment_list"]:
      temp_dict = dict() 
      temp_dict["loan_application_number"] = str(data["loan_application_number"])

      if rpdetails["instalment_status"] == 1:
        amount_collected += rpdetails["principal_component"]

        if "collection" in rpdetails:
            for col_details in rpdetails["collection"]:
              if "amount_adjusted_against_bouncing_charges" in col_details:
                temp_dict["bouncing_charges"] = str(col_details["amount_adjusted_against_bouncing_charges"])
                temp_dict["due_date"] = str(rpdetails["due_date"])
                #print temp_dict["due_date"],temp_dict["loan_application_number"],"bouncing",temp_dict["bouncing_charges"],str(col_details["date"].date())

      elif rpdetails["instalment_status"] == 2:
        temp1 = rpdetails["principal_component"]
        temp2 = rpdetails["instalment_principal_component_overdue"]
        temp3 = temp1 -temp2
        amount_collected += temp3
        
        if rpdetails["instalment_bouncing_charges_overdue"] == 0:
          if "collection" in rpdetails:
            for col_details in rpdetails["collection"]:
              if "amount_adjusted_against_bouncing_charges" in col_details:
                temp_dict["bouncing_charges"] = str(col_details["amount_adjusted_against_bouncing_charges"])
                
                temp_dict["due_date"] = str(rpdetails["due_date"])
                #print temp_dict["due_date"],temp_dict["loan_application_number"],"bouncing",temp_dict["bouncing_charges"],str(col_details["date"].date())
  if "disbursement_details" in data:
	temp_dict["loan_amount"] = data["disbursement_details"]["total_loan_amount"]
  if "loan_amount" in temp_dict:
  	temp_dict["balance_amount"] = temp_dict["loan_amount"] - amount_collected

  if data["loan_request_status"] == 11:
    temp_dict["balance_amount"] = 0
  
  temp_list.append(temp_dict)

print temp_list
