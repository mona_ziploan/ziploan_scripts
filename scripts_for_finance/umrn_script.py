from pymongo import MongoClient
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

temp_dict = dict()
temp_list = list()
loan_id_list = list()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": {"$gte": 10} }},

{"$lookup":
        {
          "from": "loan_request_business_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "business_info"
        }
 }, 
{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment"
        }
 },
{ "$unwind" : "$repayment" },
{"$lookup":
        {
          "from": "loan_request_personal_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "personal_info"
        }
 },
{ "$unwind" : "$personal_info" },

]):	
	temp_dict =  dict()
	
	
	
	if "repayment" in data:			
		temp_dict["loan_application_number"] = str(data['loan_application_number'])
		temp_dict['business_name'] = str(data["business_info"][0]["business_name"])
		temp_dict["emi"] = str(data["repayment"]["instalment_amount"])
		if "umrn" in data["repayment"]:
			temp_dict["umrn"] = str(data["repayment"]["umrn"])
		else:
			temp_dict["umrn"] = "Not available"

		if temp_dict["loan_application_number"] not in loan_id_list:
			loan_id_list.append(temp_dict["loan_application_number"])
			temp_list.append(temp_dict)


print temp_list
