from pymongo import MongoClient
import bisect 
from datetime import datetime
import traceback
import csv

# connecting to the database
connection = MongoClient()
db = connection.ziploan

sourcing_dict = {
-1: "Others",
1: "Google Search",
2: "Just Dial",
3: "Email",
4: "SMS",
5: "Facebook",
6: "Newspaper Ad",
7: "ZipLoan Android App",
8: "Referred by ZipLoan Customer",
9: "ZipLoan Employee",
10: "Top-Up",
11: "Past Customer"
}

current = datetime.now().date()
temp_dict = dict()
temp_list = list()
loan_id_list = list()
def convert(stri):
	return datetime.strptime(stri, "%d-%m-%Y")

def modify(dd):
  return dd.strftime('%d-%m-%Y')

for data in db.loan_request.aggregate([
{"$match": { "loan_request_status": {"$gte": 10} }}, 
{"$lookup":
        {
          "from": "loan_request_term_loan_repayment_details",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "repayment"
        }
 },
{ "$unwind" : "$repayment" },
{"$lookup":
        {
          "from": "loan_request_serviceable_emi",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "servicable_emi"
        }
 },
{ "$unwind" : "$servicable_emi" },
{"$lookup":
        {
          "from": "partner_details",
          "localField": "partner_id",
          "foreignField": "_id",
          "as": "partner"
        }
 },
{'$unwind': {'path': '$partner', 'preserveNullAndEmptyArrays': True}},
{"$lookup":
        {
          "from": "loan_request_zipscore_scorecard_feature_results",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "zipscore"
        }
 },
{ "$unwind" : "$zipscore" },
{"$lookup":
        {
          "from": "loan_request_serviceable_emi",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "servicable_emi"
        }
 },
{ "$unwind" : "$servicable_emi" },
{"$lookup":
        {
          "from": "loan_request_business_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "business_info"
        }
 },
{ "$unwind" : "$business_info" },
{"$lookup":
        {
          "from": "business_category",
          "localField": "business_info.business_category_id",
          "foreignField": "_id",
          "as": "category"
        }
 },
{ "$unwind" : "$category" },

{"$lookup":
        {
          "from": "loan_request_personal_info",
          "localField": "_id",
          "foreignField": "loan_request_id",
          "as": "personal_info"
        }
 },
{ "$unwind" : "$personal_info" },
{"$lookup":
        {
          "from": "loan_request_consumer_cibil_personal_data",
          "localField": "personal_info._id",
          "foreignField": "loan_Request_Personal_Info_ID",
          "as": "scorecard"
        }
 },
{ "$unwind" : "$scorecard" }

]):	
  temp_dict =  dict()
  dpd_date_list = list()
  temp_dict["no_of_emis_bounced"] = 0
  temp_dict["no_of_repayment_due"] = 0
  temp_dict["dpd_in_days"] = 0

  temp_dict["loan_application_number"] =  str(data["loan_application_number"])
  if 'personal_info' in data and 'information' in data['personal_info']:
    for ele in data['personal_info']['information']:
      #print ele
      if ele['applicant_type'] == 1:
        temp_dict["customer_name1"] = str(ele['first_name'])+" "+str(ele['last_name'])
        temp_dict["mobile"] = str(ele["mobile"])
    
      elif ele['applicant_type'] == 2:
        temp_dict["customer_name2"] = str(ele['first_name'])+" "+str(ele['last_name'])
        temp_dict["mobile"] = str(ele["mobile"])

  if  'business_info' in data:
    temp_dict["business_name"] = str(data['business_info']['business_name'])

  if "category" in data:
    temp_dict["business_category"] = str(data["category"]["category_name"])
  else:
    temp_dict["business_category"] = "Corrupted"

  if "servicable_emi" in data:
    temp_dict["business_margin"] = str(data["servicable_emi"]["business_margin"])
  else:
    temp_dict["business_margin"] = "Corrupted"

  temp_dict["zipscore"] = str(data['zipscore']['zipscore'])
  
  for sdate in data["status_date"]:
    if sdate["status"] == 5:
      temp_dict["status_5_date"] = str(modify(sdate["date"].date()))
    if sdate["status"] == 7:
      temp_dict["status_7_date"] = str(modify(sdate["date"].date()))
    if sdate["status"] == 10:
      temp_dict["status_10_date"] = str(modify(sdate["date"].date()))
    if sdate["status"] == 11:
      temp_dict["status_11_date"] = str(modify(sdate["date"].date()))

  
  if "scorecard" in data:
    if "score" in data["scorecard"] and int(data["scorecard"]["applicant_type"]) == 1:
      temp_dict["primary_cibil_score"] = str(data["scorecard"]["score"][0]["score"])
    else: 
      temp_dict["primary_cibil_score"] = "NA"

    if "score" in data["scorecard"] and int(data["scorecard"]["applicant_type"]) == 2:
      temp_dict["secondary_cibil_score"] = str(data["scorecard"]["score"][0]["score"])
    else: 
      temp_dict["secondary_cibil_score"] = "NA"

  else:
    temp_dict["primary_cibil_score"] = "NA"
    temp_dict["secondary_cibil_score"] = "NA"

  """
  if "scorecard" in data:
    for dscore in data["scorecard"]:
      print dscore["applicant_type"]

      if "applicant_type" in dscore and int(dscore["applicant_type"]) == 1:
        if "score" in dscore:
            temp_dict["primary_cibil_score"] = str(dscore["score"][-1]["score"])

      if "applicant_type" in dscore and int(dscore["applicant_type"]) == 2:
        if "score" in dscore:
            temp_dict["secondary_cibil_score"] = str(dscore["score"][-1]["score"])
  """
  if data["sourcing_type"] == 0 :
    temp_dict["sourcing_type"] = "Direct"
  else:
    temp_dict["sourcing_type"] = "Indirect"


  if "partner" in data:
    temp_dict["partner_name"]= str(data["partner"]["business_name"])

  if data["sourcing_type"] == 0 and "direct_sourcing_type" in data:
    temp_dict["direct_sourcing_type"] = str(sourcing_dict[int(data["direct_sourcing_type"])])
  else:
    temp_dict["direct_sourcing_type"] = "NA"

  if "servicable_emi" in data:
    temp_dict["annualised_business_credit"] = data["servicable_emi"]["annualised_business_credit"]
  else:
    temp_dict["annualised_business_credit"] = "NA"


  if "repayment" in data:
    for instalments in data["repayment"]["instalment_list"]:
      if instalments["instalment_status"] == 0 and convert(instalments["due_date"]).date()<current:
        bisect.insort(dpd_date_list, convert(instalments["due_date"]).date()) 
        #dpd_date_list.append(convert(instalments["due_date"]).date())
        temp_dict["no_of_repayment_due"] += 1

      if instalments["instalment_status"] == 2:
        bisect.insort(dpd_date_list, convert(instalments["due_date"]).date()) 
        temp_dict["no_of_repayment_due"] += 1

      if "collection" in instalments:
        for col_list in instalments["collection"]:
          if "status" in col_list:
            if col_list["status"] == 0:
              temp_dict["no_of_emis_bounced"]+=1
              break
  if len(dpd_date_list)>0:
    dpd_days = (current-dpd_date_list[0]).days
  else:
    dpd_days = 0

  temp_dict["dpd_in_days"] = dpd_days

  if data["loan_request_status"] == 11:
    temp_dict["dpd_in_days"] = 0
    temp_dict["no_of_repayment_due"] = 0
  
  temp_list.append(temp_dict)

print temp_list    




  
